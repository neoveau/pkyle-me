var canvas = document.querySelector("canvas");
var context = canvas.getContext("2d");
var time = 0,
  width,
  height;
var points = [];
var particles = [];
var stop = false;
var fps, fpsInterval, startTime, now, then, elapsed;

setup();

function setup() {
  resize();
  startAnimation(60); // FPS
  window.addEventListener("resize", resize);
}

function resize() {
  width = canvas.width = window.innerWidth;
  height = canvas.height = window.innerHeight;
}

function clear() {
  context.clearRect(0, 0, width, height);
}

function createParticles() {
  if (particles.length < 100) {
    particles.push({
      x: Math.random() * canvas.width,
      y: canvas.height,
      speed: -2 - Math.random() * 5,
      radius: 0.5 + Math.random() * 5,
    });
  }
}

function moveParticles() {
  for (var i in particles) {
    var part = particles[i];
    part.y += part.speed;
    if (part.y < 0) {
      part.x = Math.random() * canvas.width;
      part.y = canvas.height;
      part.speed = -2 - Math.random() * 2;
      part.radius = 0.5 + Math.random() * 5;
    }
  }
}

function startAnimation(fps) {
  fpsInterval = 1000 / fps;
  then = window.performance.now();
  startTime = then;
  render();
}

var x,
  y,
  cy = height / 2;

function render(newtime) {

  requestAnimationFrame(render);

  now = newtime;
  elapsed = now - then;

  if (elapsed > fpsInterval) {
    then = now - (elapsed % fpsInterval);
    time += 0.01

    clear();
    createParticles();
    moveParticles();

    // Bubbles
    for (var i in particles) {
      var part = particles[i];
      context.beginPath();
      context.arc(part.x, part.y, part.radius, 0, Math.PI * 2);
      context.closePath();
      context.fillStyle = "#393e46";
      context.fill();
    }
    context.strokeStyle = "#32e0c4";
    context.lineWidth = 3;
    context.beginPath();

    // Oscilloscope
    for (var i = 200; i > 0; i--) {
      var value = i * 15 + (time % 15);
      var ay = Math.sin(value * (4.45 + Math.sin(time / 2) * 0.1)) / 7;
      (y = ay * value), (x = 0 - width);
      x += Math.pow(i, 2);
      context.lineTo(x, cy + y);
      context.stroke();
      context.beginPath();
      context.moveTo(x, cy + y);
    }
    context.lineTo(x, cy + y);
    context.stroke();
  }
}
